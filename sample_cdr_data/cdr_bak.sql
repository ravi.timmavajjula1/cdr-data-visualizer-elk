-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: Lab_QA_Sonar_MVP21
-- ------------------------------------------------------
-- Server version	5.5.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `call_detail_record`
--

DROP TABLE IF EXISTS `call_detail_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `call_detail_record` (
  `accounting_entry_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Primary key is also a foreign key to an accounting entry',
  `subscription_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Subscription the call was made against',
  `invoice_id` bigint(20) unsigned DEFAULT NULL COMMENT 'The invoice the call was booked against',
  `service_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The service the call was made against',
  `actor_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The actor id of the site - will be the default site for parent calls',
  `site_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The site id - will be the default site for parent calls',
  `network_element_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The network element the call passed through',
  `customer_partition_network_element_id` int(10) unsigned DEFAULT NULL COMMENT 'The network element the customer partition is in',
  `partition_id` int(10) unsigned DEFAULT NULL COMMENT 'The partition the call was made in',
  `incoming_resource_group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The resource group the call came in on',
  `outgoing_resource_group_id` int(10) unsigned DEFAULT NULL COMMENT 'The resource group the call went out on',
  `incoming_route_id` int(10) unsigned DEFAULT NULL COMMENT 'The id of the route the call came in on',
  `outgoing_route_id` int(10) unsigned DEFAULT NULL COMMENT 'The id of the route the call went out on',
  `start_datetime` timestamp NOT NULL DEFAULT '1999-12-31 13:00:00' COMMENT 'The time the call arrived at the switch',
  `original_called_number` varchar(32) NOT NULL DEFAULT '' COMMENT 'The called number that came into the switch in the SIP INVITE',
  `translated_called_number` varchar(32) NOT NULL DEFAULT '' COMMENT 'The called number after translation in the pre-trans',
  `called_number_select` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT 'Which called number used for rating, 1 = original, 2 = translated',
  `original_calling_number` varchar(32) NOT NULL DEFAULT '' COMMENT 'The calling number that came into the switch in the SIP INVITE',
  `translated_calling_number` varchar(32) NOT NULL DEFAULT '' COMMENT 'The calling number after translation in the calling-trans',
  `calling_number_select` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT 'Which calling number used for rating, 1 = original, 2 = translated',
  `billing_party` varchar(32) DEFAULT NULL COMMENT 'The identifier of the billing party - use in conjuction with billing party type',
  `billing_party_type_id` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT 'which type of identifier was used to id subscriber',
  `diverting_number` varchar(32) DEFAULT NULL COMMENT 'The original number that initiated the diversion (if there is one)',
  `service_number` varchar(32) DEFAULT NULL COMMENT 'The number of the addressable associated with this call',
  `service_number_type_id` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'which type of number was used in the service number',
  `calling_number_presentation` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'the calling number presentation flag (0=present 1=restricted)',
  `connect_datetime` timestamp NULL DEFAULT NULL COMMENT 'Time the call was connected',
  `disconnect_datetime` timestamp NULL DEFAULT NULL COMMENT 'Time the call was disconnected',
  `seized_duration` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Call duration from seize to disconnect',
  `charged_duration` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'charged call duration in seconds',
  `release_cause_number` smallint(5) unsigned NOT NULL DEFAULT '16',
  `completion_code_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `call_direction` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'Outbound (subscriber is calling) = 1, inbound (subscriber is called) = 2',
  `active_times_id` int(10) unsigned NOT NULL DEFAULT '0',
  `destination_id` int(10) unsigned NOT NULL DEFAULT '0',
  `destination_category_id` int(10) unsigned NOT NULL DEFAULT '4',
  `call_accounting_status_id` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'Fully Charged = 1, Partially Charged = 2, No Charge = 3',
  `created_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The time the record was created',
  `re_rating_option_id` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `refunded` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Set to 1 when the call has been fully refunded',
  `hourly_cogs_summary_id` bigint(20) unsigned DEFAULT NULL,
  `hourly_margin_summary_id` bigint(20) unsigned DEFAULT NULL,
  `carrier_rate_re_rating_option_id` tinyint(3) unsigned DEFAULT '1',
  `customer_rate_version_id` int(10) unsigned DEFAULT NULL COMMENT 'Identifies the set of location and rating information used for this call',
  `originating_location_id` int(10) unsigned DEFAULT NULL COMMENT 'The originating location of the call',
  `originating_carrier_id` int(10) unsigned DEFAULT NULL COMMENT 'The carrier the call originated from',
  `originating_transit_carrier_id` int(10) unsigned DEFAULT NULL COMMENT 'The carrier the call entered the local network from, may differ from the originating carrier',
  `terminating_location_id` int(10) unsigned DEFAULT NULL COMMENT 'The terminating location of the call',
  `terminating_carrier_id` int(10) unsigned DEFAULT NULL COMMENT 'The carrier the call terminated on',
  `terminating_transit_carrier_id` int(10) unsigned DEFAULT NULL COMMENT 'The carrier the call exited the local network on, may differ from the terminating carrier',
  `capped_charged_duration` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'The portion of the call duration that is charged before the charge is capped, in seconds',
  `location_pair_category_id` int(10) unsigned DEFAULT NULL COMMENT 'A categorisation of the combination of the originating and terminating locations, used for reporting purposes',
  `parent_call_detail_record_id` bigint(20) unsigned DEFAULT NULL COMMENT 'Reference to the next CDR down the call chain',
  PRIMARY KEY (`accounting_entry_id`),
  KEY `fk_call_detail_record_invoice` (`invoice_id`),
  KEY `fk_call_detail_record_re_rating_option` (`re_rating_option_id`),
  KEY `fk_call_detail_record_hourly_cogs_summary` (`hourly_cogs_summary_id`),
  KEY `fk_call_detail_record_hourly_margin_summary` (`hourly_margin_summary_id`),
  KEY `fk_call_detail_record_carrier_rate_re_rating_option` (`carrier_rate_re_rating_option_id`),
  KEY `start_datetime` (`start_datetime`),
  KEY `subscription_id` (`subscription_id`,`start_datetime`),
  KEY `original_called_number` (`original_called_number`,`start_datetime`),
  KEY `translated_called_number` (`translated_called_number`,`start_datetime`),
  KEY `original_calling_number` (`original_calling_number`,`start_datetime`),
  KEY `translated_calling_number` (`translated_calling_number`,`start_datetime`),
  KEY `actor_id` (`actor_id`,`site_id`,`start_datetime`),
  KEY `key_actor_created_timestamp` (`actor_id`,`created_timestamp`),
  KEY `parent_call_detail_record_id` (`parent_call_detail_record_id`),
  CONSTRAINT `fk_call_detail_record_accounting_entry` FOREIGN KEY (`accounting_entry_id`) REFERENCES `accounting_entry` (`accounting_entry_id`),
  CONSTRAINT `fk_call_detail_record_carrier_rate_re_rating_option` FOREIGN KEY (`carrier_rate_re_rating_option_id`) REFERENCES `carrier_rate_re_rating_option` (`id`),
  CONSTRAINT `fk_call_detail_record_hourly_cogs_summary` FOREIGN KEY (`hourly_cogs_summary_id`) REFERENCES `hourly_cogs_summary` (`id`),
  CONSTRAINT `fk_call_detail_record_hourly_margin_summary` FOREIGN KEY (`hourly_margin_summary_id`) REFERENCES `hourly_margin_summary` (`id`),
  CONSTRAINT `fk_call_detail_record_invoice` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`invoice_id`),
  CONSTRAINT `fk_call_detail_record_re_rating_option` FOREIGN KEY (`re_rating_option_id`) REFERENCES `re_rating_option` (`id`),
  CONSTRAINT `fk_call_detail_record_site` FOREIGN KEY (`actor_id`, `site_id`) REFERENCES `site` (`actor_id`, `site_id`),
  CONSTRAINT `fk_call_detail_record_subscription` FOREIGN KEY (`subscription_id`) REFERENCES `subscription` (`subscription_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='A record of a charged voice call';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `call_detail_record`
--
-- WHERE:  accounting_entry_id>587790138

LOCK TABLES `call_detail_record` WRITE;
/*!40000 ALTER TABLE `call_detail_record` DISABLE KEYS */;
/*!40000 ALTER TABLE `call_detail_record` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-31 21:18:39
