1. Create a index schema with fields for geo_point data
	PUT cdr_data
	{
	  "mappings": {
	    "properties": {
	      "originating_location_geohash" : {
		"type": "geo_point"
	      },
	      "terminating_location_geohash" : {
		"type": "geo_point"
	      }
	    }
	  }
	}

2. List all indices
	GET _cat/indices?v

3. Delete an index
	DELETE cdr_data

4. Search an index
	GET cdr_data/_search
	{
	  "query": {"match_all": {}},
	  "size": 50,
	  "from": 0
	}

5. Check datatype mapping
	GET cdr_data/_mapping/field/originating_location_geohash

	GET cdr_data/_mapping/field/terminating_location_geohash

6. Conditions in query
	GET /cdr_data/_search
	{
	  "query": {
	    "bool": {
	      "must": [{ "match": {"originating_location_name": "SYDNEY"}}]
	    }},
	  "size": 1000
	}

	GET /bank/_search
	{
	  "query": {
	    "bool": {
	      "must": [
		{ "match": {
		  "age": "40",
		  "state": "ID"
		}}, 
		],
	      "must_not": [
		{ "match": {
		  "state": "ID"
		}}
		]
	    }
	  }
	}
