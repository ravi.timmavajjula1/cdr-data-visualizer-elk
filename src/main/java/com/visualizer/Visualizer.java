package com.visualizer;

import ch.hsr.geohash.GeoHash;
import com.visualizer.entity.Coordinates;
import com.visualizer.entity.Location;
import com.visualizer.utils.OpenStreetMapUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Logger;

class Visualizer {

    public final static Logger log = Logger.getLogger("Visualizer");

    public static void main(String[] args) {

        ArrayList<Location> locationsList = new ArrayList<>();

        String query = " UPDATE location set geohash = ? where id = ? ";


        try {
            Connection con = getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select id, name, country_id, is_detail_geo_location from location where geohash is null");
            while (rs.next()) {
                locationsList.add(new Location(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getBoolean(4)));
                System.out.println(rs.getInt(1) + "  " + rs.getString(2) + "  " + rs.getString(3) + " " + rs.getBoolean(4));
            }
            con.close();
        } catch (Exception e) {
            log.finest(e.getMessage());
        }

        for (Location location : locationsList) {
            try {

                System.out.println("Getting Coordinates for " + location.getName() + ", " + location.getCountryId() + ". ");
                Coordinates coordinates = OpenStreetMapUtils.getInstance().getCoordinates(location.getName() + ", " + location.getCountryId());

                GeoHash geoHash = GeoHash.withCharacterPrecision(coordinates.getLat(), coordinates.getLon(), 12);
                location.setGeoHash(geoHash.toBase32());
                System.out.println("GEO# :" + geoHash.toBase32());

                Connection con = getConnection();
                PreparedStatement preparedStatement = con.prepareStatement(query);
                preparedStatement.setString(1, location.getGeoHash());
                preparedStatement.setInt(2, location.getId());

                preparedStatement.execute();

                con.close();

            } catch (Exception e) {
                log.finest(e.getMessage());
                e.printStackTrace();
            }
        }
    }

    private static Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection con = DriverManager.getConnection(
                "jdbc:mysql://192.168.43.109:3306/sonar_data", "root", "root");
        con.setAutoCommit(true);
        return con;
    }
}