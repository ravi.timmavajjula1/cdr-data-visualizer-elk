package com.visualizer.entity;

public class Location {
    private int id;
    private String name;
    private String countryId;
    private boolean isDetailGeoLocation;
    private String geoHash;

    public Location(int id, String name, String countryId, boolean isDetailGeoLocation) {
        this.id = id;
        this.name = name;
        this.countryId = countryId;
        this.isDetailGeoLocation = isDetailGeoLocation;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCountryId() {
        return countryId;
    }

    public boolean isDetailGeoLocation() {
        return isDetailGeoLocation;
    }

    public String getGeoHash() {
        return geoHash;
    }

    public void setGeoHash(String geoHash) {
        this.geoHash = geoHash;
    }
}
